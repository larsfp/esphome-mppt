#                         +-----------+
#                 3.3V    |    ant    |   GND
#                 reset   |           |   gpio23
# input   adc     gpio36  |           |   gpio22
# input   adc     gpio39  |           |   internal
# input   adc     gpio34  |           |   internal
# input   adc     gpio35  |           |   gpio21
# input   adc     gpio32  |           |   GND    <- svart kabel
# input   adc     gpio33  |           |   gpio19
#                 gpio25  |           |   gpio18
#                 gpio26  |           |   gpio05
#         touch   gpio27  |           |   gpio17 <- gul kabel
#         touch   gpio14  |           |   gpio16 <- grønn kabel
#         touch   gpio12  |           |   gpio04  touch
#                 GND     |           |   gpio00  touch   output
#         touch   gpio13  |           |   gpio02  touch
#               internal  |           |   gpio15  touch
#               internal  |           |   internal
#               internal  |           |   internal
#               5V        | o       o |   internal
#                         +----USB----+

substitutions:
  device_name: mppt01
  friendly_name: "${device_name} - MPPT prototype"

external_components:
  - source: github://syssi/esphome-dps@main # https://github.com/syssi/esphome-dps

esphome:
  name: ${device_name}
  platform: ESP32
  board: nodemcu-32s # https://esphome.io/devices/nodemcu_esp32.html
  on_boot:
    priority: 600
    then:
      - switch.turn_on: mpptswitch

<<: !include credentials-gf10b.yaml
<<: !include defaults.yaml
api: # Enable Home Assistant API
  encryption:
    key: !secret mppt01key

logger:
  level: INFO
  logs:
    dps: WARN

uart:
  id: uart0
  tx_pin: 17
  rx_pin: 16
  baud_rate: 9600

modbus:
  id: modbus0
  uart_id: uart0
  send_wait_time: 0ms

dps:
  id: dps0
  modbus_id: modbus0
  update_interval: 5s

binary_sensor:
  - platform: dps
    output:
      name: "${device_name} output"
    key_lock:
      name: "${device_name} key lock"
    constant_current_mode:
      name: "${device_name} constant current mode"

sensor:
  - platform: dps
    output_voltage:
      name: "${device_name} output voltage"
      id: output_voltage
    output_current:
      name: "${device_name} output current"
    output_power:
      name: "${device_name} output power"
      id: output_power
    input_voltage:
      id: input_voltage
      name: "${device_name} input voltage"
    voltage_setting:
      name: "${device_name} voltage setting"
      id: voltage_sensor
    current_setting:
      name: "${device_name} current setting"
    backlight_brightness:
      name: "${device_name} backlight brightness"
    firmware_version:
      name: "${device_name} firmware version"
  - platform: wifi_signal
    name: "${device_name} WiFi Signal"
    update_interval: 60s
  - platform: uptime
    id: uptimesensor
    name: "${device_name} Uptime"
    update_interval: 10s

text_sensor:
  - platform: dps
    protection_status:
      name: "${device_name} protection status"
    device_model:
      name: "${device_name} device model"
  - platform: version
    name: "${device_name} ESPHome Version"  

switch:
  - platform: dps
    output:
      name: "${device_name} output"
      restore_mode: ALWAYS_OFF
      id: dps_on
    key_lock:
      name: "${device_name} key lock"
  - platform: template
    id: mpptswitch
    name: "MPPT"
    optimistic: true
    on_turn_on: 
      then:
        - lambda: |-
            id(mppt) = true;
            id(print_info).execute();

    on_turn_off: 
      then:
        - lambda: |-
            id(mppt) = false;
  - platform: restart
    name: "${device_name} Restart"
    id: restart_switch

number:
  - platform: dps
    voltage_setting:
      name: "${device_name} voltage setting"
      id: voltage_setting
    current_setting:
      name: "${device_name} current setting"
  - platform: template
    name: "${device_name} Minimum input Voltage"
    optimistic: true
    internal: false
    id: min_input
    initial_value: 8
    max_value: 23
    min_value: 1
    step: 0.5
  - platform: template
    name: "${device_name} Max output voltage"
    optimistic: true
    internal: false
    id: max_output
    initial_value: 14
    max_value: 20
    min_value: 1
    step: 0.5

globals:
  - id: mppt
    type: bool
    restore_value: no
    initial_value: 'true'
  - id: previous_power
    type: float
    initial_value: '0'
  - id: previous_voltage
    type: float
    initial_value: '0'
  - id: voltage_step
    type: float
    initial_value: '0.5'
  - id: latest_change
    type: float
    initial_value: '0'

interval:
  - interval: 11sec
    then:
      - lambda: |-
          id(print_info).execute();
      - if:
          condition:
            and:
              - switch.is_on: mpptswitch # MPPT aktivert
              - lambda: 'return id(dps_on).state || id(input_voltage).state > id(min_input).state;' # Sjekk at input_voltage er OK
          then:
            - lambda: |-
                float default_voltage = 13; // where box returns to after crash
                float new_voltage = 2;

                if (!id(dps_on).state) {  // Output is OFF

                  // if we have a good previous_voltage, use it
                  if (id(previous_voltage) != 0) {
                    new_voltage = id(previous_voltage)-1;
                  }

                  // Has crashed, reset voltage
                  if (id(voltage_sensor).state == default_voltage) {
                    ESP_LOGI("ppt", "Output is OFF. Resetting to %.1f V. Previous was %.1f V. Current setting is %.1f V.",
                      new_voltage, id(previous_voltage), id(voltage_sensor).state);
                    id(set_voltage)->execute(new_voltage);

                    // Save state, slightly cheating?
                    if (id(voltage_sensor).state != default_voltage) { id(previous_voltage) = new_voltage; }
                    id(previous_power) = id(output_power).state;

                    return;
                  }

                  ESP_LOGI("ppt", "Switching output ON.");
                  id(dps_on).turn_on();

                  // Save state
                  if (id(voltage_sensor).state != default_voltage) { id(previous_voltage) = id(voltage_sensor).state; }
                  id(previous_power) = id(output_power).state;
                  return;

                } else { // Output is ON

                  ESP_LOGI("ppt", "Output is ON");

                  ESP_LOGI("ppt", "Power is %.1f W.", id(output_power).state);

                  if (id(output_power).state < 0.1) {
                    ESP_LOGI("ppt", "Output is on, but no power. Resetting voltage.");

                    id(set_voltage)->execute(new_voltage);
                    id(dps_on).turn_on();

                    // Save state
                    if (id(output_voltage).state != default_voltage) { id(previous_voltage) = id(output_voltage).state; }
                    id(previous_power) = id(output_power).state;
                    return;
                  }


                  // Make voltage ajustments

                  // Don't go above max_output
                  if (id(voltage_setting).state >= id(max_output).state) {
                    ESP_LOGI("ppt", "Above max, stepping one voltage_step down.");
                    id(adjust_voltage)->execute(-id(voltage_step));

                    // Save state
                    if (id(output_voltage).state != default_voltage) { id(previous_voltage) = id(output_voltage).state; }
                    id(previous_power) = id(output_power).state;
                    return;
                  }

                  // Don't go above input_voltage
                  if (id(voltage_setting).state >= id(input_voltage).state-1.5) {
                    ESP_LOGI("ppt", "Above (input-1.5V), stepping one voltage_step down.");
                    id(dps_on).turn_off(); // can stop responding in this state
                    id(adjust_voltage)->execute(-id(voltage_step));

                    // Save state
                    if (id(output_voltage).state != default_voltage) { id(previous_voltage) = id(output_voltage).state; }
                    id(previous_power) = id(output_power).state;
                    return;
                  }

                  ESP_LOGI("ppt", "Increasing voltage");
                  id(adjust_voltage)->execute(id(voltage_step));

                  // Save state
                  if (id(output_voltage).state != default_voltage) { id(previous_voltage) = id(output_voltage).state; }
                  id(previous_power) = id(output_power).state;
                  return;
                }
                return;

script:
  - id: print_info
    then:
      - lambda: |-
          ESP_LOGI("print_info", "Uptime %.0f, output %d, input %.1f V, output %.1f V, power %.1f W",
            id(uptimesensor).state, id(dps_on).state, id(input_voltage).state, id(voltage_setting).state, id(output_power).state);
  - id: adjust_voltage
    parameters:
      adjustment: float
    then:
      - lambda: |-
          ESP_LOGI("adjust_voltage", "Current power: %.0f W. Adjusting voltage: %.1f V -> %.1f V.", id(output_power).state, id(voltage_setting).state, id(voltage_setting).state + adjustment);

          auto call = id(voltage_setting).make_call();
          call.set_value(id(voltage_setting).state + adjustment);
          call.perform();
  - id: set_voltage
    parameters:
      adjustment: float
    then:
      - lambda: |-
          auto call = id(voltage_setting).make_call();
          call.set_value(adjustment);
          call.perform();

          ESP_LOGI("set_voltage", "Output %d. Setting voltage: %.1f V.",
            id(dps_on).state, adjustment);
