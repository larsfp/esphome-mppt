# Esphome Mppt

## Overview

An example of creating a very basic (MPPT)[https://en.wikipedia.org/wiki/Maximum_power_point_tracking] solar controller using an (esp32)[https://en.wikipedia.org/wiki/ESP32] and a DPS5020 PSU.

This project was heavily inspired by (OSPController)[https://github.com/opensolarproject/OSPController], and partly written because that project seems abandoned. (Specifically in (issue 30)[https://github.com/opensolarproject/OSPController/issues/30]).

## Usage

Once you have (ESPHome)[https://esphome.io/] up and running, just install this on a free ESP using `esphome run mppt01.yaml`. You need a secret.yaml with an API key for esphome and home-assistant to talk.

The ESP should be connected to the DSP as described in original project.

## Dependencies

Would not have been done without the esphome module for the DPS, (esphome-dps)[https://github.com/syssi/esphome-dps]. Installation will pull in that project automatically.

## State of tracking

For now the tracking will simply attempt to power on at a minimum voltage, and scale up until output is 2V below input. This seems to give a good enough result.

Note that the DPS crashes when input can't supply what output is asking for. One workaround for that is to add the default voltage your DPS reverts to after a crash in the code, named `default_voltage`.

## Graph

![Graph showing tracking and power output](/img/tracking.png "Tracking graph")
